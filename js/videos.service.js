(function(){

     'use strict'
     function VideosService($http){
        this.getVideos=function(data){

            var url =  '/../supermariobros/handler.php';

            return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/json'}
                });
        }
     }
     
     angular
        .module('indexApp')
        .service('VideosService',VideosService)

 }());