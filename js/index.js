(function(){

     'use strict'

    function appCtrl($scope, $sce, $document, $animate, Carousel, $mdSidenav, $mdDialog, $location, VideosService,configService,$http){
         var ctrl = this;
        ctrl.$sce=$sce;
        $scope.industriaFlag = true;
        $scope.premiumFlag = true;
        $scope.llave = "";
        $scope.buscar = function(){
            var urlToRedirect='index.html#/search/'+$scope.llave;
            location.href = urlToRedirect;
        }

        $scope.toBlog = function(){
            location.href="https://www.adntalent.com/blog";
        }
        
        $scope.newTest = function(){
            console.log('entro');
            $http({
                method: 'POST',
                url: '/../supermariobros/handler.php',
                data:{
                    "model":"test",
                    "submodelo": "modeltest",
                    "action":"iniciarTestAnonimo",
                } 
            }).then(function(response){
                location.href ="https://www.adntalent.com/panel/#/test/"+ response.data; 
            })
            .catch(function(error){
                alert('No se pudo generar test')
            });
        }

        $scope.newUser = function(){
            location.href="https://www.adntalent.com/panel/#/menu";
        }

        $scope.showIndustria = function(){
           $scope.industriaFlag = !$scope.industriaFlag;
        }

        $scope.showPremium = function(){
            $scope.premiumFlag = !$scope.premiumFlag;
         }

$scope.newEnterprise = function(){
            location.href="https://www.adntalent.com/panel/#/preregister/7";
        }
        $scope.newCandidate = function(){
            location.href="https://www.adntalent.com/panel/#/preregister/8";
        }

         $scope.myInterval = 5000;
        
 
        ctrl.Videos = {
            "model":"videos",
            "submodelo": "videos",
            "action":"saveVideo",
            "url":"",
            "descripcion":""
        };
    
            ctrl.lista_videos = {
            "model":"videos",
            "submodelo": "videos",
            "action":"getVideos"
        };

         ctrl.getAviso = {
            "model":"avisos",
            "submodelo": "privacidad",
            "action":"getPrivacy"
        }



       configService.getDescripcion(ctrl.getAviso).then(function successCallback(response){
                ctrl.descripcion = response.data.t41_descripcion;
                ctrl.terminos = response.data.t41_terminos;
                ctrl.legal = response.data.t41_avisolegal;
                ctrl.derechos = response.data.t41_derechos;
        })



        VideosService.getVideos(ctrl.lista_videos).then(function successCallback (response){
            ctrl.videosResults = response.data;
          
            ctrl.videosArray=[];
            angular.forEach(ctrl.videosResults, function(value,key){
                ctrl.videos = {};
                ctrl.videos.liga = $sce.trustAsResourceUrl(value.liga); 
                ctrl.videos.descripcion = value.descripcion;
               
               ctrl.videosArray.push(ctrl.videos);
               
            })
            console.log(ctrl.videosArray);
            ctrl.pruebaliga = $sce.trustAsResourceUrl("https://www.youtube.com/embed/YBTfX6ueXVE"); 
        })
         
         // portfolio filter
        $(window).load(function(){'use strict';
            var $portfolio_selectors = $('.portfolio-filter >li>a');
            var $portfolio = $('.portfolio-items');
            $portfolio.isotope({
                itemSelector : '.portfolio-item',
                layoutMode : 'fitRows'
            });
            
            $portfolio_selectors.on('click', function(){
                $portfolio_selectors.removeClass('active');
                $(this).addClass('active');
                var selector = $(this).attr('data-filter');
                $portfolio.isotope({ filter: selector });
                return false;
            });
        });

        $scope.toggleLeft = function() {
            $mdSidenav("left").toggle();
        };
        
        $scope.close = function () {
            $mdSidenav('left').close();
        };

        $document.on('scroll', function() {
             if($document.scrollTop() >= 530){
                document.getElementById('menu-registry').style.visibility = 'visible';
            } else {
                document.getElementById('menu-registry').style.visibility = 'hidden';
             }
             if($document.scrollTop() >= 200){
                document.getElementById("scroll-up").style.transform = "translateY(0)";
            } else {
                document.getElementById("scroll-up").style.transform = "translateY(86px)";
             }
        });
        $scope.newSRC = function( element ){
            
                document.getElementById('videoYou').setAttribute('src', element+"?rel=0&amp;autoplay=1");
        }

        $scope.startTest = function(){
            window.location.assign('../panel/templates/test/hacer-test.html');
        }
        
        $scope.closedVideo = function(){
            document.getElementById('videoYou').removeAttribute('src');   
        }

        $scope.values = [
            {
                tittle: 'INTEGRIDAD PRIMERO',
                text: 'Establecer relaciones firmes sobre la ética de negocios.'
            },
            {
                tittle: 'INVOLUCRAMIENTO',
                text: 'Nuestros clientes son socios de negocios, si ellos crecen nosotros crecemos.'
            },
            {
                tittle: 'CONFIDENCIALIDAD',
                text: 'Nuestros socios de negocios pueden contar con entera seguridad que la información a la que tenemos acceso será tratada de la manera mas confidencial y responsable posible. Los aspectos organizacionales con los que trabajamos son sensibles y lo sabemos.'
            },
            {
                tittle: 'ENTUSIASMO',
                text: 'Actuando asi podemos aportar a las organizaciones con que trabajamos el entusiasmo necesario para: resolución de conflictos; puesta en marcha de planes y proyectos; ánimo y presteza orientados al crecimiento.'
            },
            {
                tittle: 'RESPONSABILIDAD SOCIAL',
                text: 'Te ayudamos a combinar los recursos, procesos y políticas para generar mejoras en la vida de las personas en tu organización y en tu entorno social inmediato.'
            }
        ];
        $scope.ethicalcode = [
            {
                icon: 'fa fa-check-circle-o md-card-image',
                headline: 'I Ganar, ganar, ganar',
                description: 'El beneficio para todas las partes involucradas es un principio rector de nuestra actuación.'
            },
            {
                icon: 'fa fa-certificate md-card-image',
                headline: 'II Se honesto con tus socios comerciales como con en tus relaciones personales.',
                description: 'Practicar la honestidad te libera de la carga moral, emocional y laboral que implica mentir.'
            },
            {
                icon: 'fa fa-gears md-card-image',
                headline: 'III Edifica y dignifica a tu equipo',
                description: 'La actitud correcta en tu quehacer laboral es creer en tus compañeros, prestar apoyo mutuo según tu función y capacidad. Cada uno es un engrane.'
            },
            {
                icon: 'fa fa-diamond md-card-image',
                headline: 'IV Se impecable con tus palabras, con tu imagen, con el orden de tu entorno',
                description: 'Si quieres recibir el mejor trato de los demás, comienza por tus palabras, imagen y el orden.'
            },
            {
                icon: 'fa  fa-clock-o  md-card-image',
                headline: 'V Llega a tiempo para terminar a tiempo',
                description: 'Respeta tu tiempo y sé respetuoso con el de los demás.'
            },
            {
                icon: 'fa fa-comments-o md-card-image',
                headline: 'VI Se claro, conciso, verifica, no hagas suposiciones y responde a todas tus solicitudes',
                description: 'Los errores y fallas de comunicación son el origen de los mas graves errores.'
            },
            {
                icon: 'fa  fa-exchange md-card-image',
                headline: 'VII Llama y acepta que te llamen la atención',
                description: 'Todos tenemos descuidos, tomar una posición madura y humilde, y un caracter firme mantiene la vela del barco en la direccion correcta.'
            },
            {
                icon: 'fa fa-legal md-card-image',
                headline: 'VIII Se responsable sin negar, culpar, justificar o victimizarte',
                description: 'Recuerda que crecer como persona implica darte cuenta de tus carencias y sobreponerte de ellas.'
            },
            {
                icon: 'fa fa-eye md-card-image',
                headline: 'IX Enfoca tus talentos en aprender de cada circunstancia, sin criticar, juzgar o condenar',
                description: 'Gastar fuerzas en actitudes negativas sólo traerá desgaste personal y al equipo, esfuerzate ser mejor.'
            },
            {
                icon: 'fa fa-heart md-card-image',
                headline: 'X Fomenta en ti la humildad y la gratitud sin quejarte',
                description: 'Estudios indican que las personas que viven agradecidas son más felices con respecto a quién se queja'
            },
            {
                icon: 'fa fa-mortar-board md-card-image',
                headline: 'XI Comprométete con el desarrollo profesional y personal',
                description: 'Los logros valiosos toman tiempo, constancia y dedicación. Toma el compromiso con tu futuro.'
            },
            {
                icon: 'fa fa-thumbs-up md-card-image',
                headline: 'XII A tu mente sólo lo bueno, lo puro y lo necesario',
                description: 'El cerebro ocupa mas de un 30% de las calorias que necesita nuestro cuerpo, y tu... ¿En qué gastas tu energia?.'
            },
            {
                icon: 'fa fa-users md-card-image',
                headline: 'XIII Busca el equilibrio en tu individualidad, la vida familiar, profesional y social',
                description: 'Al igual que comer solo un alimento genera anemia, una persona centrada en un solo aspecto de su vida tiende a ser famelica laboralmente hablando.'
            }
        ];
        $scope.customers = [
            {
                image: 'images/customers/c1.png',
            },
            {
                image: 'images/customers/c2.png',
            },
            {
                image: 'images/customers/c3.png',
            },
            {
                image: 'images/customers/c4.png',
            },
            {
                image: 'images/customers/c5.png',
            },
            {
                image: 'images/customers/c6.png',
            },
            {
                image: 'images/customers/c7.png',
            },
            {
                image: 'images/customers/c8.png',
            },
            {
                image: 'images/customers/c9.png',
            },
            {
                image: 'images/customers/c10.png',
            },
            {
                image: 'images/customers/c11.png',
            },
            {
                image: 'images/customers/c12.png',
            }
        ];
        $scope.portfolio = [
            {
                image:'images/portfolio/item1.png',
                tittle: 'Perfiles Psicometricos',
                templateUrl: './templates/project1-tmpl.html',
                category: 'pro'
            },
            {
                image:'images/portfolio/item2.jpg',
                tittle: 'Cumplimiento NOM STPS',
                templateUrl: './templates/project2-tmpl.html',
                category: 'pro pre'
            },
            {
                image:'images/portfolio/item3.jpg',
                tittle: 'Bolsa de Trabajo',
                templateUrl: './templates/project3-tmpl.html',
                category: 'pro pre cor'
            },
            {
                image:'images/portfolio/item4.png',
                tittle: 'Capacitación',
                templateUrl: './templates/project4-tmpl.html',
                category: 'pro cor'
            },
            {
                image:'images/portfolio/item5.png',
                tittle: 'Reingeniería de Procesos',
                templateUrl: './templates/project5-tmpl.html',
                category: 'pro cor'
            },
            {
                image:'images/portfolio/item6.jpg',
                tittle: 'Atracción de Talento',
                templateUrl: './templates/project6-tmpl.html',
                category: 'pro cor'
            },
            {
                image:'images/portfolio/item7.png',
                tittle: 'Coaching Personal',
                templateUrl: './templates/project7-tmpl.html',
                category: 'pre'
            },
            {
                image:'images/portfolio/item8.png',
                tittle: 'Salario Emocional',
                templateUrl: './templates/project8-tmpl.html',
                category: 'pre'
            }
        ];
        $scope.slideBlog = [
            {
                image: 'images/office2.jpg'
            },
            {
                image: 'images/2.jpg'
            },
            {
                image: 'images/intro.jpg'
            },
            {
                image: 'images/office.jpg'
            }
        ];

        $scope.privacity=[
        {
            image:'images/blog/blog1.jpg',
            tittle: "Aviso de Privacidad",
            descripcion:"Aviso de privacidad adntalent"
        }
        ];

        $scope.blogList = [
            {
                image: 'images/blog/blog1.jpg',
                tittle: 'Errores comunes en un currículo',
                description: '1. Usar siempre el mismo CV. ... si puedes expresar que es lo que la empresa puede esperar de ti en ese puesto incrementaras las posibilidades de quedarte con el empleo.',
                author: 'Carlos Hdez',
                date: 'AGOSTO 11'
            },
            {
                image: 'images/blog/blog2.jpg',
                tittle: 'Tips ante los desafíos de la empresa familiar',
                description: 'En promedio el 85% de las empresas latinoamericanas son PyME, de las cuales el 93% surgieron como Empresa Familiar, pero solo el 3% de las empresas familiares prevalecen a la tercera generación.',
                author: 'Carlos Hdez',
                date: 'AGOSTO 11'
            },
            {
                image: 'images/blog/blog3.jpg',
                tittle: 'Balanced Scorecard BSC',
                description: 'Errores Comunes en la Evaluación del Desempeño. Un BSC tiene que responder tres preguntas claves, qué procesos tengo que mejorar, qué necesidades tengo que atender en mis clientes y qué objetivos financieros tengo que lograr.',
                author: 'Carlos Hdez',
                date: 'AGOSTO 11'
            },
            {
                image: 'images/blog/blog4.jpg',
                tittle: 'Reuniones eficaces',
                description: '¿En tu empresa se padece de reunionitis? Te hacemos algunas recomendaciones. Parte de clarificar el objetivo de la reunión; Ningún viento es favorable para un barco que no sabe a dónde va, o como se dice en lenguaje naval, ningún viento es favorable para un barco al garete.',
                author: 'Carlos Hdez',
                date: 'AGOSTO 11'
            }
        ];
        $scope.signIn = function (){
            document.getElementById('signin').style.visibility = 'visible';
            document.getElementById('container-intro').style.visibility = 'hidden';
        }
        $scope.backIntro = function () {
            document.getElementById('signin').style.visibility = 'hidden';
            document.getElementById('container-intro').style.visibility = 'visible';
        }
         $scope.showDialogPortafolio = function(ev, project) {
            $scope.project = project;
            $mdDialog.show({
            controller: dialogController,
            templateUrl: project.templateUrl,
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
            locals: {
                project: $scope.project
             }
            })
        };
        $scope.showDialogRegister = function(ev) {
            $scope.project = "";
            $mdDialog.show({
            controller: dialogController,
            // templateUrl: './templates/modal-registro.html',
            // templateUrl: './templates/registro-candidato.html',
            templateUrl: './templates/dialog-registro.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
            locals: {
                project: $scope.project
             }
            })
        };
        $scope.showRegisterTest = function(ev) {
            $scope.project = "";
            $mdDialog.show({
                controller: dialogController,
                templateUrl: './templates/dialog-registro-test.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose:true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    project: $scope.project
                }
            })
        };
        function dialogController($scope, $mdDialog, project, $animate) {
            $scope.hide = function() {
              $mdDialog.hide();
            };
        
            $scope.cancel = function() {
              $mdDialog.cancel();
            };
        
            $scope.answer = function(answer) {
              $mdDialog.hide(answer);
            };
        }
    };
    angular
        .module('indexApp',['ui.bootstrap', 'duScroll', 'ui.carousel', 'ngParallax', 'ngMap', 'ui.router', 'ngMaterial'])
        .config(function($stateProvider, $urlRouterProvider) {
            $stateProvider
            // home page
             .state('home', {
                 url: '/home',
                 views : {
                     '' : {
                        templateUrl: 'templates/page-home.html'
                     },
                     'registro@home' : {
                        templateUrl : 'templates/menu-registro.html'
                     }
                 }
             })
             
             .state('home.login',{
                views : {
                    'login@': {
                        templateUrl: 'templates/inicio-sesion.html',
                    }
                }
             })
             
             .state('resetPassword', {
                url: '/login/resetPassword',
                templateUrl: 'templates/view-reiniciar-contrasena.html'
             })

             .state('blog', {
                 url: '/blog',
                 templateUrl: 'templates/page-blog.html'
             })

             .state('paquetes', {
                url: '/paquetes',
                templateUrl: 'templates/paquetes.html'
            })

             .state('detail1', {
                 url: '/blog/detail-1',
                 templateUrl: 'templates/page-detail1.html'
             })

             .state('detail2', {
                url: '/blog/detail-2',
                templateUrl: 'templates/page-detail2.html'
             })

             .state('detail3', {
                url: '/blog/detail-3',
                templateUrl: 'templates/page-detail3.html'
             })

             .state('detail4', {
                url: '/blog/detail-4',
                templateUrl: 'templates/page-detail4.html'
             })

             .state('privacity', {
                url: '/privacity',
                templateUrl: 'templates/page-privacity.html'
             })

             .state('terms', {
                url: '/terms',
                templateUrl: 'templates/page-terms.html'
             })
             .state('legal', {
                url: '/legal',
                templateUrl: 'templates/page-legal.html'
             })
             .state('rights', {
                url: '/rights',
                templateUrl: 'templates/page-rights.html'
             })

             $urlRouterProvider.otherwise('/home');
             new WOW().init();                
             
        })
        .controller('appCtrl',appCtrl)
 }());