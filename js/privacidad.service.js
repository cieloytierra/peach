(function(){

     'use strict'
     function configService($http){
        this.getDescripcion=function(data){

            var url =  '/../supermariobros/handler.php';

            return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/json'}
                });
        }
     }
     
     angular
        .module('indexApp')
        .service('configService',configService)

 }());