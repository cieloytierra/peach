<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>ADN Talent</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <script src="panel/libs/angular/angular/angular.js"></script>
  <script src="panel/js/web/indexN.js"></script>

  <link rel="stylesheet" href="panel/assets/styles/app.css">
  <link rel="stylesheet" href="panel/assets/styles/font.css">
  <link rel="stylesheet" href="panel/assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="panel/assets/bootstrap/dist/css/bootstrap.min.css">  
  <link rel="stylesheet" href="panel/assets/material-design-icons/material-design-icons.css">
  <link rel="stylesheet" href="panel/css/web/indexN.css">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="panel/assets/images/logo.png">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="panel/assets/images/logo.png">


</head>

<body ng-app="indexApp" ng-controller="appCtrl">
    <div class="main">
      <div id="main-container" class="container-fluid primary" style="overflow: visible;"> 
          <div class="navbar navbar-toggleable-sm flex-row align-items-center">
              <div class="nav-item dropdown">
                <a data-toggle="dropdown" class="nav-link" aria-expanded="false">
                  <i class="material-icons"></i>
                </a>
                <div class="dropdown-menu text-color" role="menu">
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">
                          <i class="fa fa-home"></i>
                        </div>
                        Inicio
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">                              
                          <i class="fa fa-group"></i>
                        </div>                       
                        Nosotros
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">                              
                          <i class="fa fa-suitcase"></i>
                        </div>
                        Portafolio
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        Brochure
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">                              
                          <i class="fa fa-cubes"></i>
                        </div>
                        Artículos
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">                              
                          <i class="fa fa-wrench"></i>
                        </div>
                        Herramientas
                      </a>
                      <a class="dropdown-item padding-menu" href="">
                        <div class="menu-icon">                              
                          <i class="fa fa-info"></i>
                        </div>
                        Contacto
                      </a>
                      <div class="hidden">
                        <a class="dropdown-item padding-menu" href="">
                          <div class="menu-icon">                              
                            <i class="fa fa-edit"></i>
                          </div>
                          Iniciar test
                        </a>
                        <a class="dropdown-item padding-menu" href="">
                         <div class="menu-icon">                              
                           <i class="fa fa-unlock"></i>
                         </div>
                         Iniciar sesión
                        </a>
                        <div class="dropdown-divider"></div>
                        <div class=" dropdown-item nav-item nav-link contact-email padding-menu" style="text-align: center !important; color: #fff!important;">
                          <span class="element">
                              <i class="fa fa-phone" style="color: rgba(243, 146, 0, 1) !important;"></i> +52 (492)-164-6289
                          </span>
                        </div>
                        <div class=" dropdown-item nav-item nav-link contact-social padding-menu" style="text-align: center!important;">
                        <div class="block clearfix">
                          <a href="" class="btn btn-icon btn-social rounded btn-sm">
                              <i class="fa fa-envelope" style="font-size: medium;"></i>                            
                              <i class="fa fa-envelope orange"></i>
                          </a>
                          <a href="" class="btn btn-icon btn-social rounded btn-sm">
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-facebook indigo"></i>
                          </a>
                          <a href="" class="btn btn-icon btn-social rounded btn-sm">
                              <i class="fa fa-twitter"></i>
                              <i class="fa fa-twitter light-blue"></i>
                          </a>
                          <a href="" class="btn btn-icon btn-social rounded btn-sm">
                            <i class="fa fa-youtube"></i>
                            <i class="fa fa-youtube red"></i>
                          </a>
                        </div>
                        </div>
                      </div> 
                </div>
              </div>
              <a class="navbar-brand">
                <img src="panel/img/web/logolittle.png" alt=".">
              </a>
              <ul class="nav navbar-nav text-blue-hover">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="" ng-click="showSignUpDialog($event)" onclick="registro()">
                        <span class="hidden-xs-down btn btn-sm rounded btn-outline b-danger text-u-c _600" style="padding: 0.7rem 0.5rem !important; text-transform: capitalize!important; background-color: rgba(243, 146, 0, 1) !important;">
                        Iniciar test
                        </span>
                    </a>
                </li>
                <li class="nav-item color-link signup">
                    <a class="nav-link" href="panel/">
                        <span class="hidden-xs-down btn btn-sm rounded btn-outline b-danger text-u-c _600" style="padding: 0.7rem 0.5rem !important; text-transform: capitalize!important; background-color: rgba(243, 146, 0, 1) !important;">
                        Iniciar sesión
                        </span>
                    </a>
                </li>
              </ul>
              <div class="collapse navbar-collapse" >
                <ul class="nav navbar-nav flex-row align-items-center ml-auto contact">
                        <div class="nav-item nav-link contact-email">
                            <span class="element">
                                <i class="fa fa-phone" style="font-size: medium;"></i> +52 (492)-164-6289
                            </span>
                        </div>
                            <div class="nav-item nav-link contact-social">
                              <div class="block clearfix">
                                    <a href="https://api.whatsapp.com/send?phone=524921646289&text=Deseo%20agendar%20una%20entrevista" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-whatsapp"></i>
                                        <i class="fa fa-whatsapp green"></i>
                                    </a>

                                    <a href="mailto:tualiado@adntalent.com" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-envelope" style="font-size: medium;"></i>                            
                                        <i class="fa fa-envelope orange"></i>
                                    </a>
                                    <a href="https://www.facebook.com/adntalent.iqtest/" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook indigo"></i>
                                    </a>
                                    <a href="https://twitter.com/AdnTalent" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter light-blue"></i>
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCDypRqRmXOcssijqUjs4BBQ" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-youtube"></i>
                                        <i class="fa fa-youtube red"></i>
                                    </a>
                                    </div>
                            </div>
                </ul>
              </div>
          </div>
      </div>
      <!-- Cotenido -->
      <div><br><br><br>
      <div class="text-center">
            <div class="nav m-y justify-content-center footer-menu">
                <a class="nav-link" href="">
                  <span><h3>Detalle de candidato</h3></span> 
                </a>
             </div>
      </div>
           <style type="text/css">
        .test {
    display: block;       /* iframes are inline by default */
    background: #000;
    border: none;         /* Reset default border */
    height: 768px;        /* Viewport-relative units */
    width: 100%;
}
      </style>



               <object data="http://adntalent.com/assets/positions/Candidato<?=$_GET['id']?>.pdf" type="application/pdf" class="test">
                    <iframe src="https://docs.google.com/viewer?url=http://adntalent.com/assets/positions/Candidato<?=$_GET['id']?>.pdf&embedded=true"  frameborder="0" allowfullscreen width="100%" height="780"></iframe>
                </object>

      </div>
      <div class="white r box-shadow-z0 m-b p-md footer">
		    <div class="footer p-a-md">
          <div class="footer-input">
            <form action="">
                <label>E-mail: <input type="email" placeholder="ejemplo@gmail.com" /></label> 
                <input type="submit" id="submit" value="Suscribirse" />
            </form>
          </div>
          <div class="text-center">
            <div class="nav m-y justify-content-center footer-menu">
                <a class="nav-link" href="">
                  <span>Inicio</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Nosotros</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Portafolio</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Brochure</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Artículos</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Herramientas</span> 
                </a>
                <a class="nav-link" href="">
                  <span>Contacto</span> 
                </a>
            </div>
            <div class="block clearfix">
             <a href="https://api.whatsapp.com/send?phone=524921646289&text=Deseo%20agendar%20una%20entrevista" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-whatsapp"></i>
                                        <i class="fa fa-whatsapp green"></i>
                                    </a>

                                    <a href="mailto:tualiado@adntalent.com" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-envelope" style="font-size: medium;"></i>                            
                                        <i class="fa fa-envelope orange"></i>
                                    </a>
                                    <a href="https://www.facebook.com/adntalent.iqtest/" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-facebook"></i>
                                        <i class="fa fa-facebook indigo"></i>
                                    </a>
                                    <a href="https://twitter.com/AdnTalent" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-twitter"></i>
                                        <i class="fa fa-twitter light-blue"></i>
                                    </a>
                                    <a href="https://www.youtube.com/channel/UCDypRqRmXOcssijqUjs4BBQ" target="_blank" class="btn btn-icon btn-social rounded btn-sm">
                                        <i class="fa fa-youtube"></i>
                                        <i class="fa fa-youtube red"></i>
                                    </a>

                                    
              <!-- <a href="" class="btn btn-icon btn-social rounded btn-sm">
                <i class="fa fa-facebook"></i>
                <i class="fa fa-facebook indigo"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded btn-sm">
                <i class="fa fa-twitter"></i>
                <i class="fa fa-twitter light-blue"></i>
              </a>
              <a href="" class="btn btn-icon btn-social rounded btn-sm">
                <i class="fa fa-youtube"></i>
                <i class="fa fa-youtube red"></i>
              </a> -->
            </div>
          </div>
          <div class="b b-b m-y-md"></div>
          <div class="row footer-bottom">
            <div class="col-sm-8">
              <small class="text-muted">© Copyright ADN Talent S.A de C.V 2018. Todos los Derechos Reservados.</small>
            </div>
            <div class="col-sm-4">
              <div class="text-sm-right text-xs-left text-muted">
                <strong>Aviso de Privacidad</strong>
                <!-- <a class="navbar-brand">
                    <img src="images/login.png" alt=".">
                </a> -->
              </div>
            </div>
          </div>
        </div>
	    </div>
    </div>  

      <!-- build:js scripts/app.html.js -->
  <!-- jQuery -->
    <script src="panel/libs/jquery/jquery/dist/jquery.js"></script>

    <script src="panel/libs/jquery/tether/dist/js/tether.min.js"></script>
    <script src="panel/libs/jquery/bootstrap/dist/js/bootstrap.js"></script>


  <!-- core -->
    <script src="panel/libs/jquery/underscore/underscore-min.js"></script>
    <script src="panel/libs/jquery/jQuery-Storage-API/jquery.storageapi.min.js"></script>
    <script src="panel/libs/jquery/PACE/pace.min.js"></script>

    <script src="panel/scripts/config.lazyload.js"></script>

    <script src="panel/scripts/palette.js"></script>
    <script src="panel/scripts/ui-device.js"></script>
    <script src="panel/scripts/ui-form.js"></script>
    <script src="panel/scripts/ui-include.js"></script>
    <script src="panel/scripts/ui-jp.js"></script>
    <script src="panel/scripts/ui-load.js"></script>
    <script src="panel/scripts/ui-nav.js"></script>
    <script src="panel/scripts/ui-scroll-to.js"></script>
    <script src="panel/scripts/ui-toggle-class.js"></script>

    <script src="panel/scripts/app.js"></script>

    <!-- ajax -->
    <script src="panel/libs/jquery/jquery-pjax/jquery.pjax.js"></script>
    <script src="panel/scripts/ajax.js"></script>


    <script type='text/javascript' src='adn/js/jquery.particleground.js'></script>
    <script type='text/javascript' src='adn/js/demo.js'></script>

    <script src="adn/js/jquery.js"></script>
    <script src="adn/js/bootstrap.min.js"></script>
    <script src="adn/js/jquery.isotope.min.js"></script>
    <script src="adn/js/main.js"></script>
    <script src="adn/js/wow.min.js"></script>

<script type="text/javascript">
              function registro() {
                  
                        $.ajax({
                            url: "adn/registro.php",
                            success: function(response) {
                                if(response == 0){}else{
                                    location.href ="https://adntalent.com/panel/#/test/"+response; 
                                 }
                            }
                        });
                        
            }
</script>

    
</body>
</html>   